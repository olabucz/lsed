rm(list=ls())

library(rpart)
library(datasets)

#FUNKCJE:
#funkcja zwraca procent blednych klasyfikacji:
err.rate <- function(org.class, pred.class) {
  #macierz pomylek:
  CM <- table(org.class, pred.class)
  #blad klasyfikatora:
  err <- 1 - sum(diag(CM)) / sum(CM)
  return (err)
}

#BAGGING (LDA):
#funkcja do przeprowadzanie procedury baggingu (trenowanie klasyfikatorow LDA), korzysta z funkcji bagging.own.pred do wyznaczenia bledu klasyfikacji,
#pobiera dane, na ktorych przeprowadza algorytm baggingu oraz liczbe klasyfikatorow
#zwraca wyniki klasyfikacji
bagging.own <- function(data, N) {
  #losowanie danych z oryginalnej proby:
  dane <- replicate(N, sample(1:nrow(data), rep = T))
  #tworzenie klasyfikatorow LDA:
  lda <- lapply(1:N, function(i) lda(class ~ ., data = data[dane[,i],], maxdepth = 1))
  
  tmp <- list(dane = dane)
  tmp$N <- N
  tmp$data <- data
  tmp$lda <- lda
  
  tmp1 <- bagging.own.pred(tmp, data)
  
  tmp$lda.class <- tmp1$lda.class
  tmp$votes <- tmp1$votes
  tmp$class <- tmp1$class
  tmp$err <- tmp1$err
  
  return(tmp)
}

#funkcja do przeprowadzania przewidywania za pomoca baggingu
#pobiera PU wraz z liczba klasyfikatorow oraz PT
#zwraca wyniki klasyfikacji
bagging.own.pred <- function(bag, data) {
  tmp <- list()
  
  lda.class <- sapply(1:bag$N, function(i) predict(bag$lda[[i]], newdata = data, type = "class")$class)
  votes <- t(sapply(1:nrow(lda.class), function(i) table(factor(lda.class[i,], levels = levels(data$class))))) 
  class <- factor(levels(data$class)[apply(votes, 1, which.max)], levels = levels(data$class))
  
  tmp$lda.class <- lda.class
  tmp$votes <- votes
  tmp$class <- class 
  tmp$err <- err.rate(data$class, tmp$class)
  
  return(tmp)
}

#BAGGING (DRZEWA KLASYFIKUJĄCE):
bagging.own.trees <- function(data, N) {
  dane <- replicate(N, sample(1:nrow(data), rep = T))
  trees <- lapply(1:N, function(i) rpart(class ~ ., data = data[dane[,i],], maxdepth = 1))
  
  tmp <- list(dane = dane)
  tmp$N <- N
  tmp$data <- data
  tmp$trees <- trees
  
  tmp1 <- bagging.own.pred.trees(tmp, data)
  
  tmp$trees.class <- tmp1$trees.class
  tmp$votes <- tmp1$votes
  tmp$class <- tmp1$class
  tmp$err <- tmp1$err
  
  return(tmp)
}

bagging.own.pred.trees <- function(bag, data) {
  tmp <- list()
  
  trees.class <- sapply(1:bag$N, function(i) predict(bag$trees[[i]], newdata = data, type = "class"))
  votes <- t(sapply(1:nrow(trees.class), function(i) table(factor(trees.class[i,], levels = levels(data$class)))))
  class <- factor(levels(data$class)[apply(votes, 1, which.max)], levels = levels(data$class))
  
  tmp$trees.class <- trees.class
  tmp$votes <- votes
  tmp$class <- class
  tmp$err <- err.rate(data$class, tmp$class)
  
  return(tmp)
}

###################################################################################

#pobranie zbioru iris:
data(iris)
#zmiana nazwy kolumny Species na class
colnames(iris)[colnames(iris) == "Species"] <- "class"
colnames(iris)
iris$class <- factor(iris$class)

#podzial danych na PU/PT 80/20:
#przetasowanie danych:
N <- nrow(iris)
iris <- iris[sample(1:N),]

#liczba obserwacji 80/20:
n_80 <- nrow(iris)*0.8
n_20 <- nrow(iris) - n_80

#podzial na PU i PT:
PU <- iris[1:n_80,]
PT <- iris[(1+n_80):nrow(iris),]

#liczba klasyfikatorow:
M <- c(1,2,5,10,20,50)

#KLASYFIKATORY LDA:
#bagging dla roznej liczby klasyfikatorow lda dla PU (10 realizacji):
tab <- sapply(M, function(v) replicate(10, bagging.own(PU, v)$err))

#bagging dla roznej liczby klasyfikatorow lda dla PT (10 realizacji):
tab.new <- sapply(M, function(v) replicate(10, bagging.own.pred(bagging.own(PU, v), PT)$err))

#wartosci srednie i odchylenia dla PU
tab.m <- apply(tab, 2, mean)
tab.s <- apply(tab, 2, sd)

#wartosci srednie i odchylenia dla PT
tab.new.m <- apply(tab.new, 2, mean)
tab.new.s <- apply(tab.new, 2, sd)

par(mfrow = c(1,2))
#wykresy:
errbar(M, tab.m, tab.m + tab.s, tab.m - tab.s, ylim = c(0.0, 0.1), ylab = "ulamek klasyfikacji blednych", xlab = "liczba klasyfikatorow")
errbar(M, tab.new.m, tab.new.m + tab.new.s, tab.new.m - tab.new.s, add = T, col = "red", errbar.col = "red")
legend("topright", c("klasyfikacje dla PU", "klasyfikacje dla PT"), fill=c("black", "red"))


#DRZEWA KLASYFIKUJACE:
#bagging dla roznej liczby drzew dla PU (10 realizacji):
tab.t <- sapply(M, function(v) replicate(10, bagging.own.trees(PU, v)$err))

#bagging dla roznej liczby drzew dla PT (10 realizacji):
tab.new.t <- sapply(M, function(v) replicate(10, bagging.own.pred.trees(bagging.own.trees(PU, v), PT)$err))

#wartosci srednie i odchylenia dla PU
tab.m.t <- apply(tab.t, 2, mean)
tab.s.t <- apply(tab.t, 2, sd)

#wartosci srednie i odchylenia dla PT
tab.new.m.t <- apply(tab.new.t, 2, mean)
tab.new.s.t <- apply(tab.new.t, 2, sd)

#wykresy:
errbar(M, tab.m.t, tab.m.t + tab.s.t, tab.m.t - tab.s.t,ylim = c(0.0, 0.6), ylab = "ulamek klasyfikacji blednych", xlab = "liczba klasyfikatorow")
errbar(M, tab.new.m.t, tab.new.m.t + tab.new.s.t, tab.new.m.t - tab.new.s.t, add = T, col = "red", errbar.col = "red")
legend("topright", c("klasyfikacje dla PU", "klasyfikacje dla PT"), fill=c("black", "red"))